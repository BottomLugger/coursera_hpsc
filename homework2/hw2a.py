"""
Demonstration script for quadratic interpolation.
Update this docstring to describe your code.
Modified by: Bottom Lugger
"""

import numpy as np
import matplotlib.pyplot as plt
from numpy.linalg import solve

def main():
# Set up linear system to interpolate through data points:

# Data points:
    dataPoints = np.array([[-1., 0.], [1., 4.], [2., 3.]])

# It would be better to define A in terms of the xi points.
# Doing this is part of the homework assignment.
    A = genCoefMatrix(dataPoints)
    b = genResultMatrix(dataPoints)
    
# Solve the system:
    c = solve(A,b)

    print "The polynomial coefficients are:"
    print c

# Plot the resulting polynomial:
    x = np.linspace(-2,3,1001)   # points to evaluate polynomial
    y = c[0] + c[1]*x + c[2]*x**2

    plt.figure(1)       # open plot figure window
    plt.clf()           # clear figure
    plt.plot(x,y,'b-')  # connect points with a blue line

# Add data points  (polynomial should go through these points!)
    plt.plot(dataPoints[:,0],dataPoints[:,1],'ro')   # plot as red circles
    plt.ylim(-2,8)         # set limits in y for plot

    plt.title("Data points and interpolating polynomial")
    
    plt.savefig('demo1plot.png')   # save figure as .png file

def genCoefMatrix(dataPoints):
    coefMatrix = np.zeros((3, 3))
    for i in range(3):
        coefMatrix[i, 0] = 1
        coefMatrix[i, 1] = dataPoints[i, 0]
        coefMatrix[i, 2] = dataPoints[i, 0] ** 2
    return coefMatrix

def genResultMatrix(dataPoints):
    resultMatrix = np.zeros((3, 1))
    for i in range(3):
        resultMatrix[i] = dataPoints[i, 1]
    return resultMatrix

if __name__ == "__main__":
    main()
