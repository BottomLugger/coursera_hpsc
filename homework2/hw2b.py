"""
Code for quadratic and polynomial interpolation.
Modified by: Bottom Lugger
"""

import numpy as np
import matplotlib.pyplot as plt
from numpy.linalg import solve

def quad_interp(xi, yi):
    """
    Quadratic interpolation.  Compute the coefficients of the polynomial
    interpolating the points (xi[i],yi[i]) for i = 0,1,2.
    Returns c, an array containing the coefficients of
      p(x) = c[0] + c[1]*x + c[2]*x**2.
    """

    # check inputs and print error message if not valid:

    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message

    error_message = "xi and yi should have length 3"
    assert len(xi)==3 and len(yi)==3, error_message

    # Set up linear system to interpolate through data points:

    ### Fill in this part to compute c ###
    A = np.vstack([np.ones(3), xi, xi**2]).T
    b = yi
    c = solve(A, b)

    return c
        
def plot_quad(xi, yi, figureName = "quadratic"):
    c = quad_interp(xi, yi)
    x = np.linspace(xi.min() - 1,  xi.max() + 1, 1000)
    y = c[0] + c[1] * x + c[2] * x**2
    
    plt.figure(1)       # open plot figure window
    plt.clf()           # clear figure
    plt.plot(x,y,'b-')  # connect points with a blue line

# Add data points  (polynomial should go through these points!)
    plt.plot(xi, yi,'ro')   # plot as red circles

    plt.title("Data points and interpolating polynomial")
    
    plt.savefig(figureName+'.png')   # save figure as .png file
    
def cubic_interp(xi, yi):
    """
    Quadratic interpolation.  Compute the coefficients of the polynomial
    interpolating the points (xi[i],yi[i]) for i = 0,1,2.
    Returns c, an array containing the coefficients of
      p(x) = c[0] + c[1]*x + c[2]*x**2 + c[3] * x**3.
    """

    # check inputs and print error message if not valid:

    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message

    error_message = "xi and yi should have length 4"
    assert len(xi)==4 and len(yi)==4, error_message

    # Set up linear system to interpolate through data points:

    ### Fill in this part to compute c ###
    A = np.vstack([np.ones(4), xi, xi**2, xi**3]).T
    b = yi
    c = solve(A, b)

    return c
        
def plot_cubic(xi, yi, figureName = "cubic"):
    c = cubic_interp(xi, yi)
    x = np.linspace(xi.min() - 1,  xi.max() + 1, 1000)
    y = c[0] + c[1] * x + c[2] * x**2 + c[3] * x**3
    
    plt.figure(1)       # open plot figure window
    plt.clf()           # clear figure
    plt.plot(x,y,'b-')  # connect points with a blue line

# Add data points  (polynomial should go through these points!)
    plt.plot(xi, yi,'ro')   # plot as red circles

    plt.title("Data points and interpolating polynomial")
    
    plt.savefig(figureName+'.png')   # save figure as .png file

def poly_interp(xi, yi):
    """
    Polynominal interpolate using solving schematic.
    """
    
    # check inputs and print error message if not valid:

    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message

    error_message = "xi and yi should have same length"
    assert len(xi) == len(yi), error_message

    # Set up linear system to interpolate through data points:

    ### Fill in this part to compute c ###
    dimension = len(xi)
    A = np.ones(dimension)
    for i in range(1, dimension):
        A = np.vstack([A, xi**i])
    A = A.T
    b = yi
    c = solve(A, b)
    return c

def plot_poly(xi, yi, figureName = "polynomial"):
    c = poly_interp(xi, yi)
    x = np.linspace(xi.min() - 1,  xi.max() + 1, 1000)
    y = c[0]
    for i in range(1, len(c)):
        y = y + c[i] * x ** i
    
    plt.figure(1)       # open plot figure window
    plt.clf()           # clear figure
    plt.plot(x,y,'b-')  # connect points with a blue line
    
# Add data points  (polynomial should go through these points!)
    plt.plot(xi, yi,'ro')   # plot as red circles

    plt.title("Data points and interpolating polynomial")
    
    plt.savefig(figureName+'.png')   # save figure as .png file


def test_quad1():
    """
    Test code, no return value or exception if test runs properly.
    """
    xi = np.array([-1.,  0.,  2.])
    yi = np.array([ 1., -1.,  7.])
    c = quad_interp(xi,yi)
    c_true = np.array([-1.,  0.,  2.])
    print "c =      ", c
    print "c_true = ", c_true
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)
    plot_quad(xi, yi)

def test_quad2():
    """
    Test code with singular matrix
    """
    xi = np.array([1., 1., 2.])
    yi = np.array([1., 1., 4.])
    singularMatrix = False
    try:
        c = quad_interp(xi, yi)
    except np.linalg.LinAlgError:
        print "Bad inputs that the coefficient matrix is singular"
        singularMatrix = True
    assert singularMatrix, "Matrix should be singular."

def test_cubic():
    xi = np.array([1., 2., 3., 4.])
    yi = np.array([1., 8., 27., 64.])
    c = cubic_interp(xi, yi)
    c_true = np.polyfit(xi,yi, 3)[::-1]
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)
    plot_cubic(xi, yi)

def test_poly1():
    xi = np.array([1., 2., 3., 4.])
    yi = np.array([3., 5., 12., 54.])
    c = poly_interp(xi, yi)
    c_true = np.polyfit(xi, yi, 3)[::-1]
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)
    plot_poly(xi, yi, "TestPoly1")
    
def test_poly2():
    xi = np.array([1., 2., 3., 4., 5.])
    yi = np.array([3., 5., 12., 5., -10.])
    c = poly_interp(xi, yi)
    c_true = np.polyfit(xi, yi, 4)[::-1]
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)
    plot_poly(xi, yi, "TestPoly2")
        
if __name__=="__main__":
    # "main program"
    # the code below is executed only if the module is executed at the command line,
    #    $ python demo2.py
    # or run from within Python, e.g. in IPython with
    #    In[ ]:  run demo2
    # not if the module is imported.
    print "Running test..."
    test_quad1()
    test_quad2()
    test_cubic()
    test_poly1()
    test_poly2()
    

